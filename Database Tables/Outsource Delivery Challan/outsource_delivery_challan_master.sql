CREATE TABLE `outsource_delivery_challan_master` (
  `outsource_delivery_challan_master_id` bigint NOT NULL AUTO_INCREMENT,
  `add_date_time` datetime(6) DEFAULT NULL,
  `add_user_id` varchar(255) DEFAULT NULL,
  `challan_date` date DEFAULT NULL,
  `challan_no` varchar(255) DEFAULT NULL,
  `dispatch_mode` varchar(255) DEFAULT NULL,
  `update_date_time` datetime(6) DEFAULT NULL,
  `update_user_id` varchar(255) DEFAULT NULL,
  `supplier_id` bigint DEFAULT NULL,
  PRIMARY KEY (`outsource_delivery_challan_master_id`),
  KEY `FKq52we6sddrahr5jj2wbhj6rpc` (`supplier_id`),
  CONSTRAINT `FKq52we6sddrahr5jj2wbhj6rpc` FOREIGN KEY (`supplier_id`) REFERENCES `supplier_master` (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
