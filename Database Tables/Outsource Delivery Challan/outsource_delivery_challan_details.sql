CREATE TABLE `outsource_delivery_challan_details` (
  `outsource_delivery_challan_details_id` bigint NOT NULL AUTO_INCREMENT,
  `add_date_time` datetime(6) DEFAULT NULL,
  `add_user_id` varchar(255) DEFAULT NULL,
  `update_date_time` datetime(6) DEFAULT NULL,
  `update_user_id` varchar(255) DEFAULT NULL,
  `instru_details_id` bigint DEFAULT NULL,
  `inwrd_req_id` bigint DEFAULT NULL,
  `outsource_delivery_challan_master_id` bigint DEFAULT NULL,
  PRIMARY KEY (`outsource_delivery_challan_details_id`),
  KEY `FK69ithdu73ko59b3hsyw2hh9fg` (`instru_details_id`),
  KEY `FKkg1gsq571fi81pon3p5sgi71s` (`inwrd_req_id`),
  KEY `FKadclvgydsm3cqk4hutjll8aap` (`outsource_delivery_challan_master_id`),
  CONSTRAINT `FK69ithdu73ko59b3hsyw2hh9fg` FOREIGN KEY (`instru_details_id`) REFERENCES `request_inward_instrument_details` (`instru_details_id`),
  CONSTRAINT `FKadclvgydsm3cqk4hutjll8aap` FOREIGN KEY (`outsource_delivery_challan_master_id`) REFERENCES `outsource_delivery_challan_master` (`outsource_delivery_challan_master_id`),
  CONSTRAINT `FKkg1gsq571fi81pon3p5sgi71s` FOREIGN KEY (`inwrd_req_id`) REFERENCES `inward_request_master` (`inwrd_req_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
