CREATE TABLE `payment_type_ref` (
  `payment_type_id` bigint NOT NULL AUTO_INCREMENT,
  `add_date_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `add_user_id` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `payment_type_name` varchar(255) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  PRIMARY KEY (`payment_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
