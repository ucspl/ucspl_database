CREATE TABLE `payment_invoice_junction` (
  `payment_invoice_junction_id` bigint NOT NULL AUTO_INCREMENT,
  `cleared_amount` decimal(19,2) DEFAULT NULL,
  `deduction_amount` decimal(19,2) DEFAULT NULL,
  `round_off_amount` decimal(19,2) DEFAULT NULL,
  `tds_amount` decimal(19,2) DEFAULT NULL,
  `invoice_master_id` bigint DEFAULT NULL,
  `payment_receipt_master_id` bigint DEFAULT NULL,
  `add_date_time` datetime(6) DEFAULT NULL,
  `add_user_id` varchar(255) DEFAULT NULL,
  `update_date_time` datetime(6) DEFAULT NULL,
  `update_user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`payment_invoice_junction_id`),
  KEY `FK8sy08nkfd1c56it76s87jd5mi` (`invoice_master_id`),
  KEY `FKs2ac8n4eaer2bok1o7ib063gd` (`payment_receipt_master_id`),
  CONSTRAINT `FK8sy08nkfd1c56it76s87jd5mi` FOREIGN KEY (`invoice_master_id`) REFERENCES `invoice_master` (`invoice_master_id`),
  CONSTRAINT `FKs2ac8n4eaer2bok1o7ib063gd` FOREIGN KEY (`payment_receipt_master_id`) REFERENCES `payment_receipt_master` (`payment_receipt_master_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
