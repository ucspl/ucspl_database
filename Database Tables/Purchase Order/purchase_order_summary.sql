CREATE TABLE `purchase_order_summary` (
  `purchase_summary_id` bigint NOT NULL AUTO_INCREMENT,
  `add_date_time` datetime(6) DEFAULT NULL,
  `add_user_id` varchar(255) DEFAULT NULL,
  `cgst` varchar(255) DEFAULT NULL,
  `discount_on_subtotal` decimal(19,2) DEFAULT NULL,
  `igst` varchar(255) DEFAULT NULL,
  `net` decimal(19,2) DEFAULT NULL,
  `sgst` varchar(255) DEFAULT NULL,
  `sub_total` decimal(19,2) DEFAULT NULL,
  `total` decimal(19,2) DEFAULT NULL,
  `update_date_time` datetime(6) DEFAULT NULL,
  `update_user_id` varchar(255) DEFAULT NULL,
  `purchase_order_id` bigint DEFAULT NULL,
  PRIMARY KEY (`purchase_summary_id`),
  KEY `FKr3uwpxk61y52ha74cl6ol33b5` (`purchase_order_id`),
  CONSTRAINT `FKr3uwpxk61y52ha74cl6ol33b5` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order_master` (`purchase_order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
