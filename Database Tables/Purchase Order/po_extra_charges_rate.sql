CREATE TABLE `po_extra_charges_rate` (
  `po_charges_id` bigint NOT NULL AUTO_INCREMENT,
  `charges_rate` decimal(19,2) DEFAULT NULL,
  `charges_type_id` bigint DEFAULT NULL,
  `purchase_order_id` bigint DEFAULT NULL,
  PRIMARY KEY (`po_charges_id`),
  KEY `FKbi4l6sa7eep278j1rhap29v3f` (`charges_type_id`),
  KEY `FKexoj7yymiefstpbnlhyipj04j` (`purchase_order_id`),
  CONSTRAINT `FKbi4l6sa7eep278j1rhap29v3f` FOREIGN KEY (`charges_type_id`) REFERENCES `quotation_extra_charges_ref` (`charges_type_id`),
  CONSTRAINT `FKexoj7yymiefstpbnlhyipj04j` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order_master` (`purchase_order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
