CREATE TABLE `outstanding_followup_history` (
  `followup_id` bigint NOT NULL AUTO_INCREMENT,
  `remark` varchar(255) DEFAULT NULL,
  `add_date_time` datetime(6) DEFAULT NULL,
  `add_user_id` varchar(255) DEFAULT NULL,
  `follow_up_date` date DEFAULT NULL,
  `update_date_time` datetime(6) DEFAULT NULL,
  `update_user_id` varchar(255) DEFAULT NULL,
  `invoice_master_id` bigint DEFAULT NULL,
  PRIMARY KEY (`followup_id`),
  KEY `FKerxtx5mfdn2tv1ii14mgwb0nw` (`invoice_master_id`),
  CONSTRAINT `FKerxtx5mfdn2tv1ii14mgwb0nw` FOREIGN KEY (`invoice_master_id`) REFERENCES `invoice_master` (`invoice_master_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
