CREATE TABLE `customer_terms` (
  `quotation_id` bigint NOT NULL,
  `term_id` bigint NOT NULL,
  KEY `FKk41l1acccy4wxc0ggfqv8y6f6` (`term_id`),
  KEY `FK81wje1inas3px99mmkw11o203` (`quotation_id`),
  CONSTRAINT `FK81wje1inas3px99mmkw11o203` FOREIGN KEY (`quotation_id`) REFERENCES `quotation_master` (`quotation_id`),
  CONSTRAINT `FKk41l1acccy4wxc0ggfqv8y6f6` FOREIGN KEY (`term_id`) REFERENCES `quotation_terms_ref` (`term_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
