CREATE TABLE `quotation_tax_transaction` (
  `quotation_id` bigint NOT NULL,
  `tax_transaction_id` bigint NOT NULL,
  KEY `FKt2a2qqc9gjguov0jjo7tx0rus` (`tax_transaction_id`),
  KEY `FK8iyct0wb2q5yibhqa7251vvn8` (`quotation_id`),
  CONSTRAINT `FK8iyct0wb2q5yibhqa7251vvn8` FOREIGN KEY (`quotation_id`) REFERENCES `quotation_master` (`quotation_id`),
  CONSTRAINT `FKt2a2qqc9gjguov0jjo7tx0rus` FOREIGN KEY (`tax_transaction_id`) REFERENCES `tax_transaction` (`tax_transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
