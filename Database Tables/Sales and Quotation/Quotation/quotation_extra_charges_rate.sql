CREATE TABLE `quotation_extra_charges_rate` (
  `qtn_charges_id` bigint NOT NULL AUTO_INCREMENT,
  `charges_rate` decimal(19,2) DEFAULT NULL,
  `charges_type_id` bigint DEFAULT NULL,
  `quotation_id` bigint DEFAULT NULL,
  PRIMARY KEY (`qtn_charges_id`),
  KEY `FK7l5n6a70io1nigndmkqu441pa` (`charges_type_id`),
  KEY `FK1m1i56hsln1nug5tvtm0np2jm` (`quotation_id`),
  CONSTRAINT `FK1m1i56hsln1nug5tvtm0np2jm` FOREIGN KEY (`quotation_id`) REFERENCES `quotation_master` (`quotation_id`),
  CONSTRAINT `FK7l5n6a70io1nigndmkqu441pa` FOREIGN KEY (`charges_type_id`) REFERENCES `quotation_extra_charges_ref` (`charges_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
