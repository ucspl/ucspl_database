CREATE TABLE `quotation_summary` (
  `quotation_summary_id` bigint NOT NULL AUTO_INCREMENT,
  `add_date_time` datetime(6) DEFAULT NULL,
  `add_user_id` varchar(255) DEFAULT NULL,
  `advance_payment` varchar(255) DEFAULT NULL,
  `balance_payment` varchar(255) DEFAULT NULL,
  `cgst` varchar(255) DEFAULT NULL,
  `discount_on_subtotal` varchar(255) DEFAULT NULL,
  `igst` varchar(255) DEFAULT NULL,
  `net` varchar(255) DEFAULT NULL,
  `sgst` varchar(255) DEFAULT NULL,
  `sub_total` decimal(19,2) DEFAULT NULL,
  `term_remark` varchar(255) DEFAULT NULL,
  `total` decimal(19,2) DEFAULT NULL,
  `update_date_time` datetime(6) DEFAULT NULL,
  `update_user_id` varchar(255) DEFAULT NULL,
  `payment_term_id` bigint DEFAULT NULL,
  `quotation_id` bigint DEFAULT NULL,
  PRIMARY KEY (`quotation_summary_id`),
  KEY `FKcsqvenr2bm6uu87m2s0na78qw` (`payment_term_id`),
  KEY `FKha9jg8krs1rgkhgbdpf4kikr7` (`quotation_id`),
  CONSTRAINT `FKcsqvenr2bm6uu87m2s0na78qw` FOREIGN KEY (`payment_term_id`) REFERENCES `quotation_terms_ref` (`term_id`),
  CONSTRAINT `FKha9jg8krs1rgkhgbdpf4kikr7` FOREIGN KEY (`quotation_id`) REFERENCES `quotation_master` (`quotation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
