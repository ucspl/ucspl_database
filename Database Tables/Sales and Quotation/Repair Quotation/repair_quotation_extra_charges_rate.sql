CREATE TABLE `repair_quotation_extra_charges_rate` (
  `qtn_charges_id` bigint NOT NULL AUTO_INCREMENT,
  `charges_rate` varchar(255) DEFAULT NULL,
  `charges_type_id` bigint DEFAULT NULL,
  `quotation_id` bigint DEFAULT NULL,
  PRIMARY KEY (`qtn_charges_id`),
  KEY `FKmq2nt4n2pjjk9gp4cpxh38t1b` (`charges_type_id`),
  KEY `FKebrngvw9y0rqrq1rfv33r2k30` (`quotation_id`),
  CONSTRAINT `FKebrngvw9y0rqrq1rfv33r2k30` FOREIGN KEY (`quotation_id`) REFERENCES `repair_quotation_master` (`quotation_id`),
  CONSTRAINT `FKmq2nt4n2pjjk9gp4cpxh38t1b` FOREIGN KEY (`charges_type_id`) REFERENCES `quotation_extra_charges_ref` (`charges_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
