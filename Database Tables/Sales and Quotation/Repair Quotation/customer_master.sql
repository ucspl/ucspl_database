CREATE TABLE `customer_master` (
  `cust_id` bigint NOT NULL AUTO_INCREMENT,
  `addr_line1` varchar(255) DEFAULT NULL,
  `addr_line2` varchar(255) DEFAULT NULL,
  `billing_credit_period_id` bigint NOT NULL,
  `checked_by` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `email1` varchar(255) DEFAULT NULL,
  `email2` varchar(255) DEFAULT NULL,
  `gst_no` varchar(255) DEFAULT NULL,
  `landline_no` varchar(255) DEFAULT NULL,
  `mobile_no1` varchar(255) DEFAULT NULL,
  `mobile_no2` varchar(255) DEFAULT NULL,
  `cust_name` varchar(255) DEFAULT NULL,
  `pan_no` varchar(255) DEFAULT NULL,
  `pass_fail_remark` int NOT NULL,
  `pin` bigint NOT NULL,
  `prefix_for_mob1` varchar(255) DEFAULT NULL,
  `prefix_for_mob2` varchar(255) DEFAULT NULL,
  `region_area_code_id` bigint NOT NULL,
  `sez_note` varchar(255) DEFAULT NULL,
  `status` varchar(1) NOT NULL,
  `taxpayer_type_id` bigint NOT NULL,
  `vendor_code` varchar(255) DEFAULT NULL,
  `parent_id` bigint NOT NULL,
  `add_date_time` datetime(6) DEFAULT NULL,
  `add_user_id` varchar(255) DEFAULT NULL,
  `update_date_time` datetime(6) DEFAULT NULL,
  `update_user_id` varchar(255) DEFAULT NULL,
  `is_parent` int NOT NULL,
  `country_id` bigint DEFAULT NULL,
  `state_id` bigint DEFAULT NULL,
  `cust_type` varchar(255) DEFAULT NULL,
  `company_reference_id` bigint DEFAULT NULL,
  PRIMARY KEY (`cust_id`),
  KEY `FK1t1qj6xga74i10k3v609ppluh` (`billing_credit_period_id`),
  KEY `FKt729l0dbqt9a09utk5goog9xl` (`region_area_code_id`),
  KEY `FKncy94fi0dpwcqncdk5l8af1em` (`taxpayer_type_id`),
  KEY `FK4fmuntgvslxkad63haqlto3v9` (`country_id`),
  KEY `FKmjuhbeuy1ype2sfp60sm8i2i7` (`company_reference_id`),
  KEY `FKt1j9jnphtkuomg2bka83lvp53` (`state_id`),
  CONSTRAINT `FK1t1qj6xga74i10k3v609ppluh` FOREIGN KEY (`billing_credit_period_id`) REFERENCES `billing_credit_period_ref` (`billing_credit_period_id`),
  CONSTRAINT `FK4fmuntgvslxkad63haqlto3v9` FOREIGN KEY (`country_id`) REFERENCES `country_ref` (`country_id`),
  CONSTRAINT `FKmjuhbeuy1ype2sfp60sm8i2i7` FOREIGN KEY (`company_reference_id`) REFERENCES `our_company_reference` (`company_reference_id`),
  CONSTRAINT `FKncy94fi0dpwcqncdk5l8af1em` FOREIGN KEY (`taxpayer_type_id`) REFERENCES `taxpayer_type_ref` (`taxpayer_type_id`),
  CONSTRAINT `FKt1j9jnphtkuomg2bka83lvp53` FOREIGN KEY (`state_id`) REFERENCES `states_ref` (`state_id`),
  CONSTRAINT `FKt729l0dbqt9a09utk5goog9xl` FOREIGN KEY (`region_area_code_id`) REFERENCES `region_area_code_ref` (`region_area_code_id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
