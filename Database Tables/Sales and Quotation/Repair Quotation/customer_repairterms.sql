CREATE TABLE `customer_repairterms` (
  `quotation_id` bigint NOT NULL,
  `term_id` bigint NOT NULL,
  KEY `FKevs67yurk31ckq8bu0w35d05u` (`term_id`),
  KEY `FK5q98u2jsd4570pahv124o1q41` (`quotation_id`),
  CONSTRAINT `FK5q98u2jsd4570pahv124o1q41` FOREIGN KEY (`quotation_id`) REFERENCES `repair_quotation_master` (`quotation_id`),
  CONSTRAINT `FKevs67yurk31ckq8bu0w35d05u` FOREIGN KEY (`term_id`) REFERENCES `quotation_terms_ref` (`term_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
