CREATE TABLE `repair_quotation_tax_transaction` (
  `quotation_id` bigint NOT NULL,
  `tax_transaction_id` bigint NOT NULL,
  KEY `FK5l2cxhwig8c57964din7thrmc` (`tax_transaction_id`),
  KEY `FKdmt1iu0xkau8avk8c7yxid2eo` (`quotation_id`),
  CONSTRAINT `FK5l2cxhwig8c57964din7thrmc` FOREIGN KEY (`tax_transaction_id`) REFERENCES `tax_transaction` (`tax_transaction_id`),
  CONSTRAINT `FKdmt1iu0xkau8avk8c7yxid2eo` FOREIGN KEY (`quotation_id`) REFERENCES `repair_quotation_master` (`quotation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
