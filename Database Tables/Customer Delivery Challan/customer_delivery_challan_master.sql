CREATE TABLE `customer_delivery_challan_master` (
  `customer_delivery_challan_master_id` bigint NOT NULL AUTO_INCREMENT,
  `add_date_time` datetime(6) DEFAULT NULL,
  `add_user_id` varchar(255) DEFAULT NULL,
  `challan_date` datetime(6) DEFAULT NULL,
  `challan_no` varchar(255) DEFAULT NULL,
  `dispatch_mode` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `update_date_time` datetime(6) DEFAULT NULL,
  `update_user_id` varchar(255) DEFAULT NULL,
  `group_type_id` bigint DEFAULT NULL,
  `inwrd_req_id` bigint DEFAULT NULL,
  PRIMARY KEY (`customer_delivery_challan_master_id`),
  KEY `FKqrcy6trbg9xcods4m2wbp5e3m` (`group_type_id`),
  KEY `FKpb17glurk4jadju4awv4y4lkd` (`inwrd_req_id`),
  CONSTRAINT `FKpb17glurk4jadju4awv4y4lkd` FOREIGN KEY (`inwrd_req_id`) REFERENCES `inward_request_master` (`inwrd_req_id`),
  CONSTRAINT `FKqrcy6trbg9xcods4m2wbp5e3m` FOREIGN KEY (`group_type_id`) REFERENCES `invoice_group_type_ref` (`group_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
