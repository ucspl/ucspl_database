CREATE TABLE `customer_delivery_challan_details` (
  `customer_delivery_challan_details_id` bigint NOT NULL AUTO_INCREMENT,
  `add_date_time` datetime(6) DEFAULT NULL,
  `add_user_id` varchar(255) DEFAULT NULL,
  `update_date_time` datetime(6) DEFAULT NULL,
  `update_user_id` varchar(255) DEFAULT NULL,
  `customer_delivery_challan_master_id` bigint DEFAULT NULL,
  `instru_details_id` bigint DEFAULT NULL,
  PRIMARY KEY (`customer_delivery_challan_details_id`),
  KEY `FKaamu7u77oq03gsf0h2e7n0ogw` (`customer_delivery_challan_master_id`),
  KEY `FK4uagavnbwathcpc2gqbpkst18` (`instru_details_id`),
  CONSTRAINT `FK4uagavnbwathcpc2gqbpkst18` FOREIGN KEY (`instru_details_id`) REFERENCES `request_inward_instrument_details` (`instru_details_id`),
  CONSTRAINT `FKaamu7u77oq03gsf0h2e7n0ogw` FOREIGN KEY (`customer_delivery_challan_master_id`) REFERENCES `customer_delivery_challan_master` (`customer_delivery_challan_master_id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
