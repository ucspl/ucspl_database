CREATE TABLE `invoice_request_no_junction` (
  `invoice_master_id` bigint NOT NULL,
  `inwrd_req_id` bigint NOT NULL,
  KEY `FKnvre787u22uxc0em79wre9qre` (`inwrd_req_id`),
  KEY `FKdgkymyhmmvf2h0yu3yvdbyitu` (`invoice_master_id`),
  CONSTRAINT `FKdgkymyhmmvf2h0yu3yvdbyitu` FOREIGN KEY (`invoice_master_id`) REFERENCES `invoice_master` (`invoice_master_id`),
  CONSTRAINT `FKnvre787u22uxc0em79wre9qre` FOREIGN KEY (`inwrd_req_id`) REFERENCES `inward_request_master` (`inwrd_req_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
