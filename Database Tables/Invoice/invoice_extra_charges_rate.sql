CREATE TABLE `invoice_extra_charges_rate` (
  `inv_charges_id` bigint NOT NULL AUTO_INCREMENT,
  `charges_rate` decimal(19,2) DEFAULT NULL,
  `charges_type_id` bigint DEFAULT NULL,
  `invoice_master_id` bigint DEFAULT NULL,
  PRIMARY KEY (`inv_charges_id`),
  KEY `FKobiiscceoxyguu9cw4ridd2jp` (`charges_type_id`),
  KEY `FK1to6m9xujrnbn92fg8offmlfn` (`invoice_master_id`),
  CONSTRAINT `FK1to6m9xujrnbn92fg8offmlfn` FOREIGN KEY (`invoice_master_id`) REFERENCES `invoice_master` (`invoice_master_id`),
  CONSTRAINT `FKobiiscceoxyguu9cw4ridd2jp` FOREIGN KEY (`charges_type_id`) REFERENCES `quotation_extra_charges_ref` (`charges_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
