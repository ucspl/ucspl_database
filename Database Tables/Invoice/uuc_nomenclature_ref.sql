CREATE TABLE `uuc_nomenclature_ref` (
  `instrument_id` bigint NOT NULL AUTO_INCREMENT,
  `add_date_time` datetime(6) DEFAULT NULL,
  `add_user_id` varchar(255) DEFAULT NULL,
  `instrument_name` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `update_date_time` datetime(6) DEFAULT NULL,
  `update_user_id` varchar(255) DEFAULT NULL,
  `parameter_id` bigint DEFAULT NULL,
  `instru_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`instrument_id`),
  KEY `FK32shrhwwilmwmoh4rdl8ojo2x` (`parameter_id`),
  CONSTRAINT `FK32shrhwwilmwmoh4rdl8ojo2x` FOREIGN KEY (`parameter_id`) REFERENCES `parameters_ref` (`parameter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
