CREATE TABLE `tax_transaction` (
  `tax_transaction_id` bigint NOT NULL AUTO_INCREMENT,
  `add_date_time` datetime(6) DEFAULT NULL,
  `add_user_id` varchar(255) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `tax_value` varchar(255) DEFAULT NULL,
  `tax_id` bigint DEFAULT NULL,
  `tax_transaction_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`tax_transaction_id`),
  KEY `FK88opf24p71o65rwocawhwep8q` (`tax_id`),
  CONSTRAINT `FK88opf24p71o65rwocawhwep8q` FOREIGN KEY (`tax_id`) REFERENCES `tax_ref` (`tax_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
