CREATE TABLE `invoice_tax_transaction` (
  `invoice_master_id` bigint NOT NULL,
  `tax_transaction_id` bigint NOT NULL,
  KEY `FK8xkus23cichkytxh9wc9f8esa` (`tax_transaction_id`),
  KEY `FKl9ogf0673gn1tbx6eknyrwh74` (`invoice_master_id`),
  CONSTRAINT `FK8xkus23cichkytxh9wc9f8esa` FOREIGN KEY (`tax_transaction_id`) REFERENCES `tax_transaction` (`tax_transaction_id`),
  CONSTRAINT `FKl9ogf0673gn1tbx6eknyrwh74` FOREIGN KEY (`invoice_master_id`) REFERENCES `invoice_master` (`invoice_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
