CREATE TABLE `invoice_summary` (
  `invoice_summary_id` bigint NOT NULL AUTO_INCREMENT,
  `add_date_time` datetime(6) DEFAULT NULL,
  `add_user_id` varchar(255) DEFAULT NULL,
  `discount_on_subtotal` decimal(19,2) DEFAULT NULL,
  `net` decimal(19,2) DEFAULT NULL,
  `sub_total` decimal(19,2) DEFAULT NULL,
  `total` decimal(19,2) DEFAULT NULL,
  `update_date_time` datetime(6) DEFAULT NULL,
  `update_user_id` varchar(255) DEFAULT NULL,
  `invoice_master_id` bigint DEFAULT NULL,
  `invoice_summaryid` bigint DEFAULT NULL,
  `balance_inv_amount` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`invoice_summary_id`),
  KEY `FKal920fl46ois184i6hn17a7ch` (`invoice_master_id`),
  CONSTRAINT `FKal920fl46ois184i6hn17a7ch` FOREIGN KEY (`invoice_master_id`) REFERENCES `invoice_master` (`invoice_master_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
