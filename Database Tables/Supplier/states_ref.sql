CREATE TABLE `states_ref` (
  `state_id` bigint NOT NULL AUTO_INCREMENT,
  `add_date_time` datetime(6) DEFAULT NULL,
  `add_user_id` varchar(255) DEFAULT NULL,
  `country_id` bigint NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `state_code` bigint DEFAULT NULL,
  `state_name` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `update_date_time` datetime(6) DEFAULT NULL,
  `upsate_user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`state_id`),
  KEY `FKoop6mtmgtrs4pmj7rimf60tc1` (`country_id`),
  CONSTRAINT `FKoop6mtmgtrs4pmj7rimf60tc1` FOREIGN KEY (`country_id`) REFERENCES `country_ref` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
