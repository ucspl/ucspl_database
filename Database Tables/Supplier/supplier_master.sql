CREATE TABLE `supplier_master` (
  `supplier_id` bigint NOT NULL AUTO_INCREMENT,
  `add_date_time` datetime(6) DEFAULT NULL,
  `add_user_id` varchar(255) DEFAULT NULL,
  `addr_line1` varchar(255) DEFAULT NULL,
  `addr_line2` varchar(255) DEFAULT NULL,
  `approval_status` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `contact_person1` varchar(255) DEFAULT NULL,
  `contact_person2` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gst_no` varchar(255) DEFAULT NULL,
  `mobile_no1` varchar(255) DEFAULT NULL,
  `mobile_no2` varchar(255) DEFAULT NULL,
  `pan_no` varchar(255) DEFAULT NULL,
  `prefix_for_mob1` varchar(255) DEFAULT NULL,
  `prefix_for_mob2` varchar(255) DEFAULT NULL,
  `prefix_for_phone` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `supplier_code` varchar(255) DEFAULT NULL,
  `supplier_name` varchar(255) DEFAULT NULL,
  `supplier_phone_no` varchar(255) DEFAULT NULL,
  `update_date_time` datetime(6) DEFAULT NULL,
  `update_user_id` varchar(255) DEFAULT NULL,
  `country_id` bigint DEFAULT NULL,
  `state_id` bigint DEFAULT NULL,
  `supplier_type_id` bigint DEFAULT NULL,
  PRIMARY KEY (`supplier_id`),
  KEY `FK4gt2pa0tu2w9yeodbg7p10yfn` (`country_id`),
  KEY `FKm95c0l6kfkb4uh47m9beg4a9v` (`state_id`),
  KEY `FKk7kdbv9oc7d7c749k9717o6jk` (`supplier_type_id`),
  CONSTRAINT `FK4gt2pa0tu2w9yeodbg7p10yfn` FOREIGN KEY (`country_id`) REFERENCES `country_ref` (`country_id`),
  CONSTRAINT `FKk7kdbv9oc7d7c749k9717o6jk` FOREIGN KEY (`supplier_type_id`) REFERENCES `supplier_type_ref` (`supplier_type_id`),
  CONSTRAINT `FKm95c0l6kfkb4uh47m9beg4a9v` FOREIGN KEY (`state_id`) REFERENCES `states_ref` (`state_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
