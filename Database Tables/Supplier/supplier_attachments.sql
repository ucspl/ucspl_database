CREATE TABLE `supplier_attachments` (
  `supp_attachment_id` bigint NOT NULL AUTO_INCREMENT,
  `add_date_time` datetime(6) DEFAULT NULL,
  `add_user_id` varchar(255) DEFAULT NULL,
  `file_description` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_title` varchar(255) DEFAULT NULL,
  `update_date_time` datetime(6) DEFAULT NULL,
  `update_user_id` varchar(255) DEFAULT NULL,
  `supplier_id` bigint DEFAULT NULL,
  PRIMARY KEY (`supp_attachment_id`),
  KEY `FK47kudnnf3a6lygn9b0t2wm25f` (`supplier_id`),
  CONSTRAINT `FK47kudnnf3a6lygn9b0t2wm25f` FOREIGN KEY (`supplier_id`) REFERENCES `supplier_master` (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
