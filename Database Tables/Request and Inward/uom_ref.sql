CREATE TABLE `uom_ref` (
  `uom_id` bigint NOT NULL AUTO_INCREMENT,
  `add_date_time` datetime(6) DEFAULT NULL,
  `add_user_id` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `uom_name` varchar(255) DEFAULT NULL,
  `update_date_time` datetime(6) DEFAULT NULL,
  `update_user_id` varchar(255) DEFAULT NULL,
  `parameter_id` bigint DEFAULT NULL,
  PRIMARY KEY (`uom_id`),
  KEY `FK6krsk9tgbh83o9q2ego5te2ny` (`parameter_id`),
  CONSTRAINT `FK6krsk9tgbh83o9q2ego5te2ny` FOREIGN KEY (`parameter_id`) REFERENCES `parameters_ref` (`parameter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
