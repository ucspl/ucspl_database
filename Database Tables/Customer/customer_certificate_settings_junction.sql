CREATE TABLE `customer_certificate_settings_junction` (
  `cust_id` bigint DEFAULT NULL,
  `setting_id` bigint DEFAULT NULL,
  KEY `FKe383e9o64ss0fglwf5iiwvx3m` (`setting_id`),
  KEY `FKta92v703chy4ddyx8ddx9fa9r` (`cust_id`),
  CONSTRAINT `FKe383e9o64ss0fglwf5iiwvx3m` FOREIGN KEY (`setting_id`) REFERENCES `customer_certificate_setting_ref` (`setting_id`),
  CONSTRAINT `FKta92v703chy4ddyx8ddx9fa9r` FOREIGN KEY (`cust_id`) REFERENCES `customer_master` (`cust_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
