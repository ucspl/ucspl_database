CREATE TABLE `customer_contact_list` (
  `customer_contact_list_id` bigint NOT NULL AUTO_INCREMENT,
  `cust_id` bigint NOT NULL,
  `department` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `job_title` varchar(255) DEFAULT NULL,
  `landline_no` varchar(255) DEFAULT NULL,
  `mobile_no1` varchar(255) DEFAULT NULL,
  `mobile_no2` varchar(255) DEFAULT NULL,
  `person_name` varchar(255) DEFAULT NULL,
  `status` varchar(1) NOT NULL,
  PRIMARY KEY (`customer_contact_list_id`),
  KEY `FK9vca8v6kcthd2tl89462v9fq2` (`cust_id`),
  CONSTRAINT `FK9vca8v6kcthd2tl89462v9fq2` FOREIGN KEY (`cust_id`) REFERENCES `customer_master` (`cust_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
