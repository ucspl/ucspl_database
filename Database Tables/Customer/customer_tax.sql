CREATE TABLE `customer_tax` (
  `cust_id` bigint DEFAULT NULL,
  `tax_id` bigint DEFAULT NULL,
  KEY `FK3ofb5otviptk6b695iic33dw0` (`tax_id`),
  KEY `FKd8ulftpjy04sg4ma2e55m94uh` (`cust_id`),
  CONSTRAINT `FK3ofb5otviptk6b695iic33dw0` FOREIGN KEY (`tax_id`) REFERENCES `tax_ref` (`tax_id`),
  CONSTRAINT `FKd8ulftpjy04sg4ma2e55m94uh` FOREIGN KEY (`cust_id`) REFERENCES `customer_master` (`cust_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
