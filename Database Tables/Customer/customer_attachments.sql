CREATE TABLE `customer_attachments` (
  `cust_attachment_id` bigint NOT NULL AUTO_INCREMENT,
  `add_date_time` datetime(6) DEFAULT NULL,
  `add_user_id` varchar(255) DEFAULT NULL,
  `file_description` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_title` varchar(255) DEFAULT NULL,
  `update_date_time` datetime(6) DEFAULT NULL,
  `update_user_id` varchar(255) DEFAULT NULL,
  `cust_id` bigint DEFAULT NULL,
  `attached_file_name` varchar(255) DEFAULT NULL,
  `document_name` varchar(255) DEFAULT NULL,
  `original_file_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`cust_attachment_id`),
  KEY `FK16w2v00lmngo1tf91tvx5y7wk` (`cust_id`),
  CONSTRAINT `FK16w2v00lmngo1tf91tvx5y7wk` FOREIGN KEY (`cust_id`) REFERENCES `customer_master` (`cust_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
