CREATE TABLE `taxpayer_type_ref` (
  `taxpayer_type_id` bigint NOT NULL AUTO_INCREMENT,
  `add_date_time` datetime(6) DEFAULT NULL,
  `add_user_id` varchar(255) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `taxpayer_type_name` varchar(255) DEFAULT NULL,
  `update_date_time` datetime(6) DEFAULT NULL,
  `update_user_id` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`taxpayer_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
