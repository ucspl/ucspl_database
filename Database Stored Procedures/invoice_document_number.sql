DELIMITER $$
CREATE DEFINER=`ucspl_dev`@`%` PROCEDURE `invoice_document_number`(
out invDocNo varchar(255)  )
BEGIN

DECLARE finantialMonth  int;
DECLARE systemDateMonth int;

DECLARE systemYear int;
DECLARE nextYear int;
DECLARE currNextYear varchar(5);

DECLARE prefix varchar(10);
DECLARE count int; 

SET prefix='UCSPL_INV_';
SET count=0;
SET systemDateMonth = (SELECT (CAST((SELECT DATE_FORMAT(CURDATE(),'%m')) AS UNSIGNED)));    
SET finantialMonth = (SELECT (CAST((SELECT SUBSTRING(from_date,4,2)from financial_year_ref where status='A') AS UNSIGNED))) ;

if( finantialMonth = 1) then
  SET currNextYear = (SELECT DATE_FORMAT(CURDATE(),'%Y')); 
  
else
  SET systemYear = (SELECT DATE_FORMAT(CURDATE(),'%y')); 
  SET nextYear = systemYear + 1 ;
  SET currNextYear = concat(systemYear,nextYear);
         
  if (finantialMonth > systemDateMonth) then 
     SET systemYear = systemYear - 1; 
     SET nextYear = systemYear + 1 ;
     SET currNextYear = concat(systemYear,nextYear);
  end if;
     
end if;	
SET count = ( select count(*) from invoice_master i where i.inv_document_number like CONCAT('%',currNextYear,'%'))+1;
SET invDocNo =concat(prefix,currNextYear,"_",count);
END$$
DELIMITER ;
