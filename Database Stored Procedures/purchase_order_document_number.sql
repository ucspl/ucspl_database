DELIMITER $$
CREATE DEFINER=`ucspl_dev`@`%` PROCEDURE `purchase_order_document_number`(
in purchaseOrderDate date,
out poDocNo varchar(25)
)
BEGIN
DECLARE prefix varchar(10);
DECLARE poDate varchar(10);
DECLARE count int; 

SET prefix='PO_';
SET count=0;
SET poDate = DATE_FORMAT(purchaseOrderDate,'%Y%m');
SET count=(SELECT count(*) from purchase_order_master
				  where  DATE_FORMAT(po_date,'%Y%m') = DATE_FORMAT(purchaseOrderDate,'%Y%m') ) +1;
SET poDocNo =concat(prefix,poDate,"_",count);

END$$
DELIMITER ;
