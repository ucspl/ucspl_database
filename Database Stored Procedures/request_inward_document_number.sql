DELIMITER $$
CREATE DEFINER=`ucspl_dev`@`%` PROCEDURE `request_inward_document_number`(

in inwardDate date,
in inwardType int,
out reqInwDocNo varchar(25)

)
BEGIN
DECLARE count int; 
DECLARE startSeries int;
DECLARE prefix varchar(10);
DECLARE inwDate varchar(10);
DECLARE stringCount varchar(10);
DECLARE intCount int;

SET prefix='UCSPL/';
SET count=0;

CASE inwardType
		WHEN  1 THEN
		   SET startSeries = 1;
		WHEN 2 THEN
		  SET startSeries = 101;
		WHEN 3 THEN
		   SET startSeries = 301;
		WHEN 4 THEN
		   SET startSeries = 501;
		ELSE
		   SET startSeries = 701;
	   END CASE;
    
SET count=(SELECT count(*) from inward_request_master where inwrd_req_date=inwardDate group by inward_type_id having inward_type_id=inwardType);

if(count !=null OR count !='undefined')
THEN   /*  CONVERT(id, CHAR(50)) ...  CAST(PROD_CODE AS UNSIGNED)*/
SET count = count + startSeries;
ELSE 
SET count=0+startSeries;
END IF;


SET stringCount =  lpad(CONVERT(count , CHAR(50)) , 3 , 0);

SET inwDate=(select DATE_FORMAT(inwardDate,'%y%m%d'));

SET reqInwDocNo =concat(prefix,inwDate,"/",stringCount);

END$$
DELIMITER ;
