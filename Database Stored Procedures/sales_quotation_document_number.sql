DELIMITER $$
CREATE DEFINER=`ucspl_dev`@`%` PROCEDURE `sales_quotation_document_number`(
in quotationDate date,
out salesDocNo varchar(25)
)
BEGIN
DECLARE qtnDate varchar(10);
DECLARE systemDate varchar(10);
DECLARE prefix varchar(10);
DECLARE count int; 

SET prefix='UCSPL_QTN_';
SET count=0;
SET systemDate = (SELECT DATE_FORMAT(CURDATE(),'%Y%m')); 
SET qtnDate = DATE_FORMAT(quotationDate,'%Y%m');

SET count=(SELECT count(*) from quotation_master
				  where  DATE_FORMAT(quotation_date,'%Y%m') = DATE_FORMAT(quotationDate,'%Y%m') ) +1;

SET salesDocNo =concat(prefix,qtnDate,"_",count);

END$$
DELIMITER ;
