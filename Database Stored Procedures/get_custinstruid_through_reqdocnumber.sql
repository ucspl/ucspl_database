DELIMITER $$
CREATE DEFINER=`ucspl_dev`@`%` PROCEDURE `get_custinstruid_through_reqdocnumber`(
IN doc_no VARCHAR(255)
)
BEGIN

SELECT 
    instru_details_id,
    inward_instrument_no,
    n.instrument_name,
    p.parameter_name,
    id_no,
    make,
    sr_no,
    range_from,
    range_to,
    range_uom,
    least_count_uom,
    least_count,
    accuracy,
    formula_accuracy,
    uom_accuracy,
    accuracy1,
    formula_accuracy1,
    uom_accuracy1,
    location,
    calibration_frequency
FROM
   request_inward_instrument_details i
        INNER JOIN
    uuc_nomenclature_ref n ON n.instrument_id = i.instrument_id
        INNER JOIN
    parameters_ref p ON p.parameter_id = i.parameter_id
        INNER JOIN
    inward_request_master m ON m.inwrd_req_id = i.inwrd_req_id
WHERE
    m.inwrd_req_document_number = doc_no;



END$$
DELIMITER ;
